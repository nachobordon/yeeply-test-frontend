<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Repository\InMemoryArticleRepository;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
        
    /**
     * @Route("/articles", name="getArticles")
     * @param Request $request
     *
     * @return Response
     */
    public function getArticleAction()
    {
        $articleRepository = new InMemoryArticleRepository();
        $articles = $articleRepository->findAll();
        
        //$serializedArticles = $this->get('serializer')->serialize($articles, 'json');
        $serializer = $this->get('jms_serializer');
        $serializedArticles = $serializer->serialize($articles, 'json');
        
        return new Response($serializedArticles, 200, array('Content-Type' => 'application/json'));
        
    }
}
