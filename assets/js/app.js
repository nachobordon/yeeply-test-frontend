require('../css/app.scss');

import Vue from 'vue'
import App from './components/App.vue'
import BootstrapVue from 'bootstrap-vue'

const imagesContext = require.context('../images', true, /\.(png|jpg|jpeg|gif|ico|svg|webp)$/);
imagesContext.keys().forEach(imagesContext);

Vue.use(BootstrapVue);
var VueTruncate = require('vue-truncate-filter')
Vue.use(VueTruncate)

new Vue({
    el: '#app',
    components: { App }
})

